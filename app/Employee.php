<?php
namespace app;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  /**
    * The attributes that are mass assignable.
    * @var array
    */
  protected $fillable = [
    	'nombre',
      'apellido_p', 
      'apellido_m', 
      'correo', 
      'telefono', 
      'cargo',
    ];

  /**
    * The attributes excluded from the model's JSON form.
    * @var array
    */
  protected $hidden = [

  ];

  public function user()
  {
    return $this->hasOne('App\User'); // FK: employee_id // PK: id
  }
}
?>