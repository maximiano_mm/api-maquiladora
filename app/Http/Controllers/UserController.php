<?php
namespace App\Http\Controllers;

use App\User;
use App\Employee;

use Illuminate\Http\Request;

class UserController extends Controller
{
	public function index()
	{
		$users = User::all();
		return response()->json([$users], 200);
	}

	public function show($id)
	{
		$user = User::find($id);
		return response()->json([$user], 200);
	}

	public function store(Request $request)
	{
		$user = new User();
		$user->employee_id = $request->input('employee_id');
		$user->usuario = $request->input('usuario');
		$user->contrasena = $request->input('contrasena');
		$user->rol = $request->input('rol');

		$user->save();

		return response()->json([$user], 201);

 	}

 	public function update($id, Request $request)
 	{
 		$user = User::findOrFail($id);
 		$user->fill($request->all());
    $user->save();

    // Regresa un mensaje de retorno
    return response()->json(['message' => 'User updated succesfully'], 200);
 	}

 	public function destroy($id)
 	{
 		$user = User::findOrFail($id);
 		$user->delete();

 		// Regresa un mensaje de retorno
    return response()->json(['message' => 'User deleted succesfully'], 200);
 	}
}
?>