<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

  /**
   * Muestra el listado completo de los empleados
   * @return \Illuminate\Http\Response
   */
  public function index() {

    // Selecciona todos los empleados de la tabla paginados cada 10 registros
    $employees = Employee::simplePaginate(10);

    // Regresa a los empleados en un JSON
    return response()->json([$employees], 200);
  }

  /**
    * Muestra un empleado en específico
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function show($id) {

    // Encuentra al empleado por su $id
    $employee = Employee::find($id);

    // Regresa al empleado en un JSON
    return response()->json([$employee], 200);
  }

  /**
    * Almacena un nuevo empleado en la BD
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request) {

    // Valida que todos los campos esten formateados
    $this->validate($request, [
      'nombre' => 'required|max: 20',
      'apellido_p' => 'required|max: 15',
      'apellido_m' => 'required|max: 15',
      'correo' => 'required|email|max: 30',
      'telefono' => 'required|max: 15',
      'cargo' => 'required|max: 15'
    ]);

    // Captura los datos del request y los guarda
    $employee = new Employee();
    $employee->nombre = $request->input('nombre');
    $employee->apellido_p = $request->input('apellido_p');
    $employee->apellido_m = $request->input('apellido_m');
    $employee->correo = $request->input('correo');
    $employee->telefono = $request->input('telefono');
    $employee->cargo =$request->input('cargo');
    $employee->save();

    // Regresa un mensaje de retorno
    return response()->json([$employee], 201);

  }

  /**
    * Actualiza un empleado en específico de la BD.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update($id, Request $request) {

    // Valida que todos los campos esten formateados
    $this->validate($request, [
      'nombre' => 'required|max: 20',
      'apellido_p' => 'required|max: 15',
      'apellido_m' => 'required|max: 15',
      'correo' => 'required|email|max: 30',
      'telefono' => 'required|max: 15',
      'cargo' => 'required|max: 15'
    ]);

    // Encuentra y actualiza un empleado
    $employee = Employee::findOrFail($id);
    $employee->fill($request->all());
    $employee->save();

    // Regresa un mensaje de retorno
    return response()->json(['message' => 'Employee updated succesfully'], 200);
  }

  /**
    * Remueve un empleado en específico de la BD.
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy($id) {

    // Encuentra y elimina un empleado
    $employee = Employee::findOrFail($id);

    /*$employee = Employee::join('users', function ($join) use($id) {
      $join->on('users.employee_id', '=', 'employees.id')->where('employees.id', '=', $id);
    })->delete();*/

    $employee->delete();

    // Regresa un mensaje de retorno
    return response()->json(['message' => 'Deleted succesfully'], 200);
  }
}
