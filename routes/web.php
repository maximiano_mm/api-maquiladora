<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return str_random(32);
});
 
 /**
  * API Routes
  */
$router->group(['prefix' => 'api/v1'], function () use ($router) {
	// Empleados CRUD endpoints
	$router->get('/employees', ['uses' => 'EmployeeController@index']);
	$router->get('/employees/{id}', ['uses' => 'EmployeeController@show']);
	$router->post('/employees', ['uses' => 'EmployeeController@store']);
	$router->put('/employees/{id}', ['uses' => 'EmployeeController@update']);
	$router->delete('/employees/{id}', ['uses' => 'EmployeeController@destroy']);

	// Usuarios CRUD endpoints
	$router->get('/users', ['uses' => 'UserController@index']);
	$router->get('/users/{id}', ['uses' => 'UserController@show']);
	$router->post('/users', ['uses' => 'UserController@store']);
	$router->put('/users/{id}', ['uses' => 'UserController@update']);
	$router->delete('/users/{id}', ['uses' => 'UserController@destroy']);
	//$router->get('/users', function() { return 'Hola';});
	/*$router->get('/user', function () {
		$user = App\User::find(1)->employee;
		return response()->json($user);
	});*/
});
