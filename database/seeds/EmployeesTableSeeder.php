<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
    	foreach (range(1,31) as $index) {
	        DB::table('employees')->insert([
	            'nombre' => $faker->firstname,
	            'apellido_p' => $faker->lastname,
	            'apellido_m' => $faker->lastname,
	            'correo' => $faker->buildingNumber,
	            'telefono' => $faker->tollFreePhoneNumber,
	            'cargo' => $faker->streetSuffix
	        ]);
    	}
  	}	
}
